#[derive(Clone, Copy)]
pub struct Particle {

    position: [f32; 4],
    velocity: [f32; 4]
}

implement_vertex!(Particle, position); // , velocity
