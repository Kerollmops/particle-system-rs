use particle::Particle;
use opencl::{Buffer, Context as CLContext, Device, CommandQueue, Program as CLProgram, MemoryAccess, Event };
use glium::vertex::VertexBuffer;
use glium::backend::Facade;
use glium::GlObject;

const KERNEL_INIT_SPHERE: &'static str = include_str!("kernels/init_sphere.cl");

pub struct Particles<'a> {

    facade: &'a Facade,
    particles_vbo: VertexBuffer<Particle>,

    context: &'a CLContext,
    device: &'a Device,
    queue: &'a CommandQueue,
    particles_buff: Buffer<Particle>,

    init_sphere_program: CLProgram,
    init_cube_program: CLProgram,
    update_program: CLProgram
}

impl<'a> Particles<'a> {

    pub fn new<F: Facade>(context: &'a CLContext, device: &'a Device, facade: &'a F, queue: &'a CommandQueue, number: usize) -> Particles<'a> {

        let vbo = VertexBuffer::empty_dynamic(facade, number).unwrap();
        let particles_buff = Buffer::from_gl_buffer(context, vbo.get_id(), number, MemoryAccess::ReadWrite);

        let init_sphere_program = CLProgram::new(context, KERNEL_INIT_SPHERE);
        let init_cube_program = CLProgram::new(context, KERNEL_INIT_SPHERE);
        let update_program = CLProgram::new(context, KERNEL_INIT_SPHERE);

        init_sphere_program.build(device).unwrap();
        init_cube_program.build(device).unwrap();
        update_program.build(device).unwrap();

        Particles{
            facade: facade,
            particles_vbo: vbo,

            context: context,
            device: device,
            queue: queue,
            particles_buff: particles_buff,

            init_sphere_program: init_sphere_program,
            init_cube_program: init_cube_program,   // NEW KERNEL HERE !
            update_program: update_program          // HERE TOO !
        }
    }

    pub fn init(&self) {

        self.queue.enqueue_acquire_gl_buffer(&self.particles_buff, None);

        // call init opencl kernel

        self.queue.enqueue_release_gl_buffer(&self.particles_buff, None);
    }
}
