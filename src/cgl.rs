use libc;

// typedef struct _CGLContextObject *CGLContextObj;
pub type CGLContextObj = *mut libc::c_void;
pub type CGLShareGroupObj = *mut libc::c_void;

// #if defined (__APPLE__) || defined(MACOSX)
// # define CL_GL_SHARING_EXT "cl_APPLE_gl_sharing"
// #else
// # define CL_GL_SHARING_EXT "cl_khr_gl_sharing"
// #endif

pub const CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE: i64 = 0x10000000;

extern
{
    pub fn CGLGetCurrentContext() -> CGLContextObj;

    pub fn CGLGetShareGroup(context: CGLContextObj) -> CGLShareGroupObj;
}
